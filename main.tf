
terraform {
  required_version = ">1.1"

  backend "s3" {
    bucket         = "kojitechs.3.tier.arch.dej"
    dynamodb_table = "terraform-state-block"
    region         = "us-east-1"
    key            = "path/env"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  vpc_id = aws_vpc.kojitechs_vpc.id
  azs = data.aws_availability_zones.azs.names
}

data "aws_availability_zones" "azs" {
    state = "available"
}

#Creating vpc
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block           = var.aws_vpc
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "kojitechs_vpc"
  }
}

#Creating Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    "name" = "gw"
  }
}

#creating Public Subnet
resource "aws_subnet" "public_subnet" {
    count = length(var.public_cidr) #to calculate the size of public sider variable 

    vpc_id = local.vpc_id
    cidr_block = var.public_cidr[count.index]
    availability_zone = element(slice(local.azs,0,2), count.index)
    map_public_ip_on_launch = true

  tags = {
    main = "public_subnet_${count.index + 1}"
  }
}

#creating Private Subnet
resource "aws_subnet" "private_subnet" {
    count = length(var.private_cidr) #to calculate the size of public sider variable 

    vpc_id = local.vpc_id
    cidr_block = var.private_cidr[count.index]
    availability_zone = element(slice(local.azs,0,2), count.index)
    map_public_ip_on_launch = true

  tags = {
    main = "private_subnet_${count.index + 1}"
  }
}

#creating Database Subnet
resource "aws_subnet" "database_subnet" {
    count = length(var.database_cidr) #to calculate the size of public sider variable 

    vpc_id = local.vpc_id
    cidr_block = var.database_cidr[count.index]
    availability_zone = element(slice(local.azs,0,2), count.index)
    map_public_ip_on_launch = true

  tags = {
    main = "database_subnet_${count.index + 1}"
  }
}